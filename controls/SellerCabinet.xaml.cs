﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WarehouseManager.Classes;

namespace WarehouseManager.controls
{
    /// <summary>
    /// Interaction logic for SellerCabinet.xaml
    /// </summary>
    public partial class SellerCabinet : UserControl, INotifyPropertyChanged
    {
        public ObservableCollection<User> Users => MainWindow.usersCollection;
        public ObservableCollection<Order> UserOrders => UsersTable.SelectedItem is null ? null : ((User)UsersTable.SelectedItem).Orders;


        public SellerCabinet()
        {
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }

        private void UsersTable_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            OnPropertyChanged("UserOrders");
        }

        
    }
}
