﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WarehouseManager.controls
{
    /// <summary>
    /// Interaction logic for EditProductsPanel.xaml
    /// </summary>
    public partial class EditProductsPanel : UserControl
    {
        MainWindow mainWindow => Application.Current.MainWindow as MainWindow;

        public EditProductsPanel()
        {
            InitializeComponent();
        }


        // Обработчик события нажатия кнопки изменить товар.
        private void EditProductButton_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.tabControl.SelectedIndex = 0;
            Product selectedProduct = (Product)mainWindow.ProductsTable.SelectedItem;
            var addProductWindow = new AddNewProductWindow(title: "Изменить товар", nameTextBoxValue: selectedProduct.Name,
                                                           articleTextBoxValue: selectedProduct.ArticleNumber, isEdit: true,
                                                           countTextBoxValue: selectedProduct.Count, priceTextBoxValue: selectedProduct.Price);
            if (addProductWindow.ShowDialog() == true)
            {
                selectedProduct.Name = addProductWindow.Name;
                selectedProduct.Price = addProductWindow.Price;
                selectedProduct.Count = addProductWindow.Count;
            }
        }

        // Обработчик события нажатия кнопки удалить товар.
        private void DeleteProductButton_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.tabControl.SelectedIndex = 0;
            Product selectedProduct = (Product)mainWindow.ProductsTable.SelectedItem;
            selectedProduct.Delete();
        }

        // Обработчик события нажатия на кнопку добавления товара.
        private void AddProductButton_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.tabControl.SelectedIndex = 0;
            if (mainWindow.ActiveSection == null)
            {
                MessageBox.Show("Не выбрано раздела, в которую нужно добавить товар. Для выбора раздела, дважды нажмите по нему слева.",
                                "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else if (mainWindow.ActiveSection.Parent == null) // Значит, что это корневой раздел.
            {
                MessageBox.Show("Невозможно добавить товар в корневой раздел.",
                                "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var addProductWindow = new AddNewProductWindow();
            if (addProductWindow.ShowDialog() == true)
            {
                mainWindow.ActiveSection.TryAddProduct(addProductWindow.Name, addProductWindow.ArticleNumber,
                                            addProductWindow.Price, addProductWindow.Count, mainWindow.AllProducts);
            }
        }

        // Изменилось выделение в таблице, значит нужно активировать/декактивировать кнопки
        // Удаления и изменения товара.
        public void ProductsTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                EditProductButton.Opacity = 0.9;
                DeleteProductButton.Opacity = 0.9;
                EditProductButton.IsEnabled = true;
                DeleteProductButton.IsEnabled = true;
            }
            else
            {
                EditProductButton.Opacity = 0.5;
                DeleteProductButton.Opacity = 0.5;
                EditProductButton.IsEnabled = false;
                DeleteProductButton.IsEnabled = false;

            }
        }
    }
}
