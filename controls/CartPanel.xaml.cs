﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WarehouseManager.Classes;

namespace WarehouseManager.controls
{
    /// <summary>
    /// Interaction logic for CartPanel.xaml
    /// </summary>
    public partial class CartPanel : UserControl
    {
        public static event Action<Product> ItemsInCart_Changed;
        public static event Action<Order> UserOrders_Changed;

        MainWindow mainWindow => Application.Current.MainWindow as MainWindow;
        public int CountItemsInCart => MainWindow.activeUser.Cart.Count;

        public CartPanel()
        {
            InitializeComponent();
        }

        private void GoToCartButton_Click(object sender, RoutedEventArgs e)
        {

        }

        // Обработчик события нажатия кнопку удалить товар из корзины.
        private void DeleteFromCartButton_Click(object sender, RoutedEventArgs e)
        {
            var product = (Product)MainWindow.clientCabinet.ProductsTable.SelectedItem;
            if (product is null)
            {
                MessageBox.Show("Сначала нужно выбрать товар", "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (MainWindow.activeUser.Cart.Count == 0)
            {
                MainWindow.cartPanel.PlaceOrderButton.IsEnabled = false;
                MainWindow.cartPanel.PlaceOrderButton.Opacity = 0.3;
            }

            MainWindow.activeUser.Cart.Remove(product);
            ItemsInCart_Changed(product);

            var text = (TextBlock)GoToCartButton.Template.FindName("CountInCartTextBlock", GoToCartButton);
            text.Text = CountItemsInCart.ToString();
        }

        // Обработчик события нажатия кнопку добавить товар в корзину.
        private void AddToCartButton_Click(object sender, RoutedEventArgs e)
        {
            Product product = (Product)mainWindow.ProductsTable.SelectedItem;
            if (product is null)
            {
                MessageBox.Show("Сначала нужно выбрать товар", "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            MainWindow.cartPanel.PlaceOrderButton.IsEnabled = true;
            MainWindow.cartPanel.PlaceOrderButton.Opacity = 1;

            MainWindow.activeUser.Cart.Add(new Product
            {
                ArticleNumber = product.ArticleNumber,
                Count = 1,
                Name = product.Name,
                Price = product.Price,
                section = product.section
            });
            ItemsInCart_Changed(product);

            // Это нужно сделать через биндинги, но времени не хватает.
            var text = (TextBlock)GoToCartButton.Template.FindName("CountInCartTextBlock", GoToCartButton);
            text.Text = CountItemsInCart.ToString();
        }

        private void PlaceOrderButton_Click(object sender, RoutedEventArgs e)
        {
            if (MainWindow.activeUser.Cart.Count == 0)
            {
                MessageBox.Show("Сначала нужно добавить в корзину товары", "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            MainWindow.cartPanel.PlaceOrderButton.IsEnabled = false;
            MainWindow.cartPanel.PlaceOrderButton.Opacity = 0.3;

            var productsInCart = new ObservableCollection<Product>();
            foreach (var product in MainWindow.activeUser.Cart)
            {
                productsInCart.Add(product);
            }

            var newOrder = new Order(MainWindow.activeUser, MainWindow.activeUser.Cart);

            MainWindow.activeUser.Orders.Add(newOrder);
            // У него будут отображаться все заказы.
            MainWindow.vendorUser.Orders.Add(newOrder);

            MainWindow.activeUser.Cart = new ObservableCollection<Product>();
            ItemsInCart_Changed(null);
            UserOrders_Changed(newOrder);

            var text = (TextBlock)GoToCartButton.Template.FindName("CountInCartTextBlock", GoToCartButton);
            text.Text = CountItemsInCart.ToString();
        }
    }
}
