﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.Specialized;

namespace WarehouseManager.controls
{
    /// <summary>
    /// UserControl, отвечающий за отображения дерева разделов (секций (каталогов)).
    /// </summary>
    public partial class TreeStructureControl : UserControl
    {

        public static event Action<ClassificationSection> ActiveSectionChanged;


        // Сами разделы, забинжены с HierarhicalDataTemplate.
        private ObservableCollection<ClassificationSection> classificationSections = new ObservableCollection<ClassificationSection>();
        public ObservableCollection<ClassificationSection> ClassificationSections
        {
            get => classificationSections;
            set
            {
                classificationSections = value;
            }
        }

        public TreeStructureControl()
        {
            InitializeComponent();
            ClassificationSections.Add(new ClassificationSection { Name = "Склад" }); // Корневой узел.

            DataContext = this;
        }

        // Обработчик события, когда в контекстном меню захотели добавить раздел.
        private void AddClassificationSection_Click(object sender, RoutedEventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var contextMenu = (ContextMenu)menuItem.Parent;
            var target = (RadioButton)contextMenu.PlacementTarget;

            var addSectionWindow = new AddNewSectionWindow();

            if (addSectionWindow.ShowDialog() == true)
            {
                (target.DataContext as ClassificationSection).TryAddChild(addSectionWindow.Name);
            }
        }

        // Обработчик события, когда в контекстном меню захотели удалить раздел.
        private void RemoveClassificationSection_Click(object sender, RoutedEventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var contextMenu = (ContextMenu)menuItem.Parent;
            var target = (RadioButton)contextMenu.PlacementTarget;

            (target.DataContext as ClassificationSection).TryDelete();
        }

        // Обработчик события, когда в контекстном меню захотели переименовать раздел.
        private void RenameClassificationSection_Click(object sender, RoutedEventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var contextMenu = (ContextMenu)menuItem.Parent;
            var target = (RadioButton)contextMenu.PlacementTarget;

            var renameSectionWindow = new AddNewSectionWindow("Переименовать раздел");

            if (renameSectionWindow.ShowDialog() == true)
            {
                (target.DataContext as ClassificationSection).TryRename(renameSectionWindow.Name);
            }
        }

        // Обработчик события выбора секции, нужно иницировать событие, на которые подписан метод в MainWindow (он изменяет таблицу с товарами).
        private void Section_MouseClick(object sender, MouseButtonEventArgs e)
        {
            var sectionTextBox = sender as RadioButton;
            ActiveSectionChanged((ClassificationSection)sectionTextBox.DataContext);
        }
    }
}
