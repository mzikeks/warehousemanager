﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WarehouseManager.Classes;

namespace WarehouseManager.controls
{
    /// <summary>
    /// Interaction logic for ClientCabinet.xaml
    /// </summary>
    public partial class ClientCabinet : UserControl, INotifyPropertyChanged
    {
        public ObservableCollection<Product> ItemsInCart => MainWindow.activeUser is null ? null: MainWindow.activeUser.Cart;
        public ObservableCollection<Order> UserOrders => MainWindow.activeUser is null ? null : MainWindow.activeUser.Orders;

        public ClientCabinet()
        {
            InitializeComponent();
            CartPanel.ItemsInCart_Changed += ItemsInCart_Changed;
            CartPanel.UserOrders_Changed += UserOrders_Changed;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }

        private void ItemsInCart_Changed(Product newProduct)
        {
            OnPropertyChanged("ItemsInCart");
         
        }

        private void UserOrders_Changed(Order newOrder)
        {
            OnPropertyChanged("UserOrders");
        }
    }
}
