﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WarehouseManager.windows;

namespace WarehouseManager.controls
{
    /// <summary>
    /// Interaction logic for ExportNecessaryProductsControl.xaml
    /// </summary>
    public partial class ExportNecessaryProductsControl : UserControl
    {
        MainWindow mainWindow => Application.Current.MainWindow as MainWindow;

        public ExportNecessaryProductsControl()
        {
            InitializeComponent();
        }

        // Обработчик нажатия на кнопку экспорта недостающих товаров (которых меньше определенного числа на складе).
        private void ExportNecessaryProductsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var saveFileDIalog = new SaveFileDialog
                {
                    Filter = "CSV Table Files (*.csv)|*.csv",
                };
                if (saveFileDIalog.ShowDialog() == true)
                {
                    string filename = saveFileDIalog.FileName;

                    using (StreamWriter writer = new StreamWriter(new FileStream(filename, FileMode.Create, FileAccess.Write), Encoding.GetEncoding(1251)))
                    {
                        writer.WriteLine("sep=,");

                        foreach (var product in mainWindow.AllProducts)
                        {
                            if (product.Count <= mainWindow.MinProdutsToExport)
                            {
                                // Экспортируем в нужном формате
                                writer.WriteLine($"\"{product.ClassificatorPath}\", " +
                                                 $"\"{product.ArticleNumber}\", \"{product.Name}\", {product.Count}");
                            }
                        }
                    }

                    MessageBox.Show("Таблица успешно сохранена", "Успех!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Не удалось выгрузить таблицу в файл. Возможно, вы делаете что-то не так." +
                                "Скорее всего, программе не удается получить доступ к файлу для записи. Попробуйте его закрыть",
                                "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

        }

        // Обработчик нажатия на кнопку настроек экспорта заканчивающихся товаров в csv.
        private void NecessaryProductsSettingsButton_Click(object sender, RoutedEventArgs e)
        {
            var settingsWindow = new NecessaryProdutsSettingsWindow(mainWindow.MinProdutsToExport);
            if (settingsWindow.ShowDialog() == true)
            {
                mainWindow.MinProdutsToExport = settingsWindow.Count;
            }
        }
    }


}
