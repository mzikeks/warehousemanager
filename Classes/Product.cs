﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager
{
    /// <summary>
    /// Класс, отвечающий за данные товара, который хранится на складе.
    /// </summary>
    [DataContract(IsReference = true)]
    public class Product: INotifyPropertyChanged
    {
        // Нужно для корректного Binding с ProductsTable.
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }

        // Все поля, вроде бы, очевидны.
        [DataMember]
        public ClassificationSection section { get; set; }

        [DataMember]
        private string name;
        public string Name
        {
            get => name;
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }


        [DataMember]
        private string articleNumber;
        public string ArticleNumber
        {
            get => articleNumber;
            set
            {
                articleNumber = value;
                OnPropertyChanged("ArticleNumber");
            }
        }

        [DataMember]
        private double price;
        public double Price
        {
            get => price;
            set
            {
                price = value;
                OnPropertyChanged("Price");
            }
        }


        [DataMember]
        private int count;
        public int Count
        {
            get => count;
            set
            {
                count = value;
                OnPropertyChanged("Count");
            }
        }

        public string ClassificatorPath
        {
            get
            {
                return string.Join("/", section.ParentNames) + "/" + section.Name;
            }
        }

        // Удаление этого товара.
        internal void Delete()
        {
            section.Products.Remove(this);
            section.AllProductsInSubsections.Remove(this);

            Console.WriteLine(section.Name);

            // Из списка всех товаров в подесекциях каждого родителя тоже удаляем.
            var sectionParent = section.Parent;
            while (sectionParent != null)
            {
                sectionParent.AllProductsInSubsections.Remove(this);
                sectionParent = sectionParent.Parent;
            }
        }
    }
}
