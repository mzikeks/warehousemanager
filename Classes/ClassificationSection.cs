﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Collections.Specialized;
using System.Windows;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WarehouseManager
{
    /// <summary>
    /// Класс, отвечающий за узел в дереве товаров. 
    /// </summary>
    [DataContract(IsReference = true)]
    public class ClassificationSection: INotifyPropertyChanged
    {
        // Нужно для корректного Binding в TreeStructureControl.
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }

        [DataMember]
        public ClassificationSection Parent { get; set; } = null;

        // Те продукты, которые будут отображаться в таблице слева.
        // Зависит от того, поствлена ли галочка в чекбоксе.
        public bool ShowAllProductsInSubsections = false;
        public ObservableCollection<Product> ShownProducts
        {
            get
            {
                if (ShowAllProductsInSubsections)
                {
                    return AllProductsInSubsections;
                }
                return Products;
            }
        }

        [DataMember]
        public ObservableCollection<ClassificationSection> Children { get; set; } = new ObservableCollection<ClassificationSection>();
        // Собственно товары, которые лежат в этой секции.
        [DataMember]
        public ObservableCollection<Product> Products { get; set; } = new ObservableCollection<Product>();
        
        [DataMember]
        private string name;
        public string Name 
        {
            get => name;
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        // Имена всех родителей, т. е. путь (для удобной выгрузки в csv).
        public List<string> ParentNames 
        {
            get
            {
                List<string> names = new List<string>();
                var parent = Parent;
                while (parent != null)
                {
                    names.Add(parent.Name);
                    parent = parent.Parent;
                }
                names.Reverse();
                return names;
            }
                
        }

        // Все товары в подсекциях текущей секции. Так удобнее, чем каждый раз при выборе проходить иерархию, хранятся же всё равно ссылки :)
        [DataMember]
        public ObservableCollection<Product> AllProductsInSubsections = new ObservableCollection<Product>();


        public ClassificationSection() 
        {
            // Нужно выписать все продукты из подсекций.
            GetProductsFromChildrens(this, AllProductsInSubsections);
        }


        // Рекурсивеый метод для записи всех товаров из подсекций.
        private void GetProductsFromChildrens(ClassificationSection section, ObservableCollection<Product> productsList)
        {
            foreach (Product product in section.Products)
            {
                productsList.Add(product);
            }

            foreach (ClassificationSection child in section.Children)
            {
                GetProductsFromChildrens(child, productsList);
            }
        }

        // Вызывается из TreeStructureControl, когда нужно добавить подсекцию.
        internal bool TryAddChild(string name)
        {
            if (!ContainsSectionName(Children, name))
            {
                var classificatorPath = new List<string>();
                foreach (var parentName in ParentNames)
                {
                    classificatorPath.Add(parentName);
                }
                classificatorPath.Add(this.Name);

                var newSection = new ClassificationSection 
                { 
                    Name = name,
                    Parent = this,
                };
                Children.Add(newSection);
                return true;
            }
            MessageBox.Show("Раздел с указанным именем уже существует.", "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
            return false;
        }

        // Вызывается из TreeStructureControl, при попытке переименовать секцмю.
        internal void TryRename(string newName)
        {
            if (this.Parent == null || !ContainsSectionName(this.Parent.Children, newName))
            {
                this.Name = newName;
            }
        }

        // Вызывается из TreeStructureControl при попытке удаления раздела.
        internal bool TryDelete()
        {
            if (Parent is null)
            {
                MessageBox.Show("Невозможно удалить корневой раздел.",
                                "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            if (Children.Count != 0 || Products.Count != 0)
            {
                MessageBox.Show("Не удалось удалить указнный раздел, так как он содержит продукты и/или подразделы.",
                                "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            Parent.Children.Remove(this);
            return true;
        }


        // Проверяет, есть ли у родительского раздела раздел с заданным именем.
        private static bool ContainsSectionName(ObservableCollection<ClassificationSection> children, string name)
        {
            foreach (var element in children)
            {
                if (element.Name == name) return true;
            }
            return false;
        }

        // Проверяет, есть ли товар в переданной коллекции. Можно было бы сделать словарем.
        private static bool ContainsProductArticle(ObservableCollection<Product> products, string article)
        {
            foreach (var element in products)
            {
                if (element.ArticleNumber == article) return true;
            }
            return false;
        }

        // Вызывается из MainWindow при попытке добавления товара.
        internal bool TryAddProduct(string name, string articleNumber, double price, int count, ObservableCollection<Product> allProducts)
        {
            if (ContainsProductArticle(allProducts, articleNumber))
            {
                MessageBox.Show("Не удалось добавить товар, так так товар с таким артикулом уже существует на складе.", "Ошибка!",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            // Нужно созать путь к продукту.
            var classificatorPath = new List<string>();
            foreach (var parentName in ParentNames)
            {
                classificatorPath.Add(parentName);
            }
            classificatorPath.Add(this.Name);

            var product = new Product
            {
                ArticleNumber = articleNumber,
                Name = name,
                Price = price,
                Count = count,
                section = this
            };
            Products.Add(product);
            AllProductsInSubsections.Add(product);

            // Нужно добаить в разделы, которые выше в иерархии.
            ClassificationSection parent = Parent;
            while (parent != null)
            {
                parent.AllProductsInSubsections.Add(product);
                parent = parent.Parent;
            }
            return true;
        }
    }
}
