﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager.Classes
{
    [DataContract(IsReference = true)]
    public class User
    {
        // Все поля, вроде бы, очевидны.
        [DataMember]
        public string FIO { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember]
        public string Adress { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public ObservableCollection<Product> Cart { get; set; } = new ObservableCollection<Product>();
        [DataMember]
        public ObservableCollection<Order> Orders { get; set; } = new ObservableCollection<Order>();

        // Попытка войти в аккаунт.
        internal static User TryLogin(string email, string password)
        {
            if (!MainWindow.users.ContainsKey(email))
            {
                throw new ArgumentException("Ошибка! Пользователя с указанным email адресом не существует");
            }

            var user = MainWindow.users[email];
            if (!user.CheckPassword(password))
            {
                throw new ArgumentException("Ошибка! Введен неверный пароль");
            }

            return user;
        }

        private bool CheckPassword(string password)
        {
            return Password == password;
        }

        // Попытка создать аккаунт.
        internal static User TryCreate(string name, string phoneNumber, string adress, string email, string password)
        {
            if (MainWindow.users.ContainsKey(email))
            {
                throw new ArgumentException("Ошибка! Пользователь с указанным email адресом уже существует");
            }

            return new User
            {
                FIO = name,
                PhoneNumber = phoneNumber,
                Adress = adress,
                Email = email,
                Password = password
            };

        }
    }
}
