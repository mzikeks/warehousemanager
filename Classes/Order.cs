﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace WarehouseManager.Classes
{

    [DataContract(IsReference = true)]
    public class Order : INotifyPropertyChanged
    {
        // Нужно для корректного Binding с ClientOrdersTable.
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }

        public static int nextId = 1000;

        // Возможные статусы заказа.
        public static readonly Dictionary<string, OrderStatus> Statuses = new Dictionary<string, OrderStatus> 
        { {"Создан", OrderStatus.Create},
          {"Обработан", OrderStatus.Processed},
          {"Оплачен", OrderStatus.Payed},
          {"Отгружен", OrderStatus.Shipped},
          {"Исполнен", OrderStatus.Finished}};

        [DataMember]
        public ObservableCollection<Product> Products { get; set; } = new ObservableCollection<Product>();
        [DataMember]
        public DateTime OrderTime { get; set; }
        [DataMember]
        public User Client { get; set; }

        [DataMember]
        private OrderStatus _orderStatus = OrderStatus.Create;
        public OrderStatus orderStatus 
        {
            get => _orderStatus;
            set 
            {
                _orderStatus = value;
                OnPropertyChanged("_orderStatus");
                OnPropertyChanged("orderStatus");
                OnPropertyChanged("OrderStatusText");
            } 
        }

        public string OrderStatusText 
        { 
            get 
            {
                List<string> text = new List<string>();
                foreach (var statusName in Statuses.Keys)
                {
                    if (orderStatus.HasFlag(Statuses[statusName]))
                    {
                        text.Add(statusName);
                    }
                }
                return String.Join(", ", text);
            }
            set { }
        }

        [DataMember]
        public double SummaryPrice { get; set; } = 0;
        [DataMember]
        public int Id { get; private set; }
        
        public Order(User client, ObservableCollection<Product> products)
        {
            orderStatus = OrderStatus.Create;
            OrderTime = DateTime.Now;
            Products = products;
            Client = client;
            Id = nextId++;

            foreach (var product in Products)
            {
                // Каждого продукта по одному.
                SummaryPrice += product.Price;
            }
        }

    }


    [Flags]
    public enum OrderStatus
    {
        Create = 1,
        Processed = 2,
        Payed = 4,
        Shipped = 8,
        Finished = 16
    }


    public class OrderStatusConverter : IValueConverter
    {
        private OrderStatus target;

        public OrderStatusConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            OrderStatus mask = (OrderStatus)parameter;
            this.target = (OrderStatus)value;
            return (mask == 0 && this.target == 0) ? true : ((mask & this.target) != 0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            target ^= (OrderStatus)parameter;
            return target;
        }
    }
}
