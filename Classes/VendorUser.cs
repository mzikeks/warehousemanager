﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseManager.Classes
{

    [DataContract(IsReference = true)]
    public class VendorUser : User
    {
        public VendorUser() 
            : base() 
        { }
            
    }
}
