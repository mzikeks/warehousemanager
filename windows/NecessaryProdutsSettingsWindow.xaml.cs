﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseManager.windows
{
    /// <summary>
    /// Окно, вызываемое при настройке минимального значения числа товаров, для их экспорта в csv.
    /// </summary>
    public partial class NecessaryProdutsSettingsWindow : Window
    {
        public NecessaryProdutsSettingsWindow(int minProdutsToExport)
        {
            InitializeComponent();
            // Устанавливаем предыдущее установленное значение. 
            CountBox.Text = minProdutsToExport.ToString();
        }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            if (CountBox.Text.Length == 0)
            {
                MessageBox.Show("Поле не может быть пустым", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            this.DialogResult = true;
        }

        // Свойство, будет использоваться для установки нужного числа.
        public int Count
        {
            get 
            {
                return int.Parse(CountBox.Text);
            }
        }

        // Обработчик события ввода текста в TextBox, нужен для фильтрации вводимых данных.
        private void CountBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textBox = sender as TextBox;

            if (!int.TryParse(textBox.Text.Insert(textBox.CaretIndex, e.Text), out int iValue) || iValue < 1)
            {
                e.Handled = true;
            }
        }

        // Предыдущий обработчик не отлавливает пробелы, они попадут сюда, и мы их поймаем.
        private void CountBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space) e.Handled = true;
        }
    }
}
