﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseManager
{
    /// <summary>
    /// Окно, всплывающее при нажатии на кнопку добавления товара.
    /// </summary>
    public partial class AddNewProductWindow : Window
    {
        // Аргументы нужны, если товар будет "изменяться" (удаляться, и добавляться новый).
        public AddNewProductWindow(string title = "Добавить товар", string nameTextBoxValue = "Новый товар", int countTextBoxValue = 3,
                                   string articleTextBoxValue = "E1412412", double priceTextBoxValue = 1000.5, bool isEdit = false)
        {
            InitializeComponent();
            Title = title;
            NameBox.Text = nameTextBoxValue;
            if (isEdit)
            {
                ArticleBox.IsEnabled = false;
            }
            ArticleBox.Text = articleTextBoxValue;
            CountBox.Text = countTextBoxValue.ToString();
            PriceBox.Text = priceTextBoxValue.ToString();
        }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            if (CountBox.Text.Length == 0 || PriceBox.Text.Length == 0 || NameBox.Text.Length == 0 || ArticleBox.Text.Length == 0)
            {
                MessageBox.Show("Одно из полей пустое, заполните, пожалуйста, все поля", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            this.DialogResult = true;
        }

        // Свойства, в которых лежат текст из текстБоксов, нужны для генерации товара.
        public new string Name
        {
            get { return NameBox.Text; }
        }

        public string ArticleNumber
        {
            get { return ArticleBox.Text; }
        }

        public double Price 
        {
            get { return double.Parse(PriceBox.Text); }
        }

        public int Count
        {
            get { return int.Parse(CountBox.Text); }
        }

        // Нужен для того, чтобы не давать пользователю вводить что-то не то в TextBox с ценой.
        private void PriceBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            // Проверяем, можно ли будет преобразовать получившийся текст в double.
            if (!double.TryParse(textBox.Text.Insert(textBox.CaretIndex, e.Text), out double iValue) || iValue < 0)
            {
                e.Handled = true;
            }
        }

        // Нужен для того, чтобы не давать пользователю вводить что-то не то в TextBox с количеством.
        private void CountBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            // Проверяем, можно ли будет преобразовать получившийся текст в int.
            if (!int.TryParse(textBox.Text.Insert(textBox.CaretIndex, e.Text), out int iValue) || iValue < 1)
            {
                e.Handled = true;
            }
        }

        // Пробелы, введенные в TextBox'ы почему-то не вызывают событие PreviewTextInput, отлавливаем их здесь.
        private void DigitBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space) e.Handled = true;
        }
    }
}
