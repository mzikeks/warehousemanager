﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseManager
{
    /// <summary>
    /// Окно, всплывающее при попытке добавить новый раздел.
    /// </summary>
    public partial class AddNewSectionWindow : Window
    {
        public AddNewSectionWindow(string title = null)
        {
            if (title != null)
            {
                this.Title = title;
            }
            InitializeComponent();
        }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            if (NameBox.Text.Length == 0)
            {
                MessageBox.Show("Поле не может быть пустым", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            this.DialogResult = true;
        }

        // Свойство, потом будет использоваться для добавления раздела.
        public new string Name
        {
            get { return NameBox.Text; }
        }
    }
}
