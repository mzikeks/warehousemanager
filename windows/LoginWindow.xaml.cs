﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseManager.windows
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        // Проверка на корректность заполнения полей.
        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            if (Email.Length == 0 || Password.Length == 0)
            {
                MessageBox.Show("Поле не может быть пустым", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            this.DialogResult = true;
        }

        public string Email
        {
            get { return EmailBox.Text; }
        }

        public string Password
        {
            get { return PasswordBox.Text; }
        }
    }
}
