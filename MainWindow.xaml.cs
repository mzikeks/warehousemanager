﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using WarehouseManager.windows;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using WarehouseManager.Classes;
using WarehouseManager.controls;

namespace WarehouseManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public static User activeUser = null;

        public static VendorUser vendorUser = new VendorUser
        {
            Adress = "Адрес магазна: улица Пушкина, дом 17",
            Email = "vendor@warehouse.ru",
            Password = "ven17dor",
            PhoneNumber = "+74950001214",
            FIO = "Продавец Продавцов",
        };

        public static Dictionary<string, User> users = new Dictionary<string, User> { { vendorUser.Email, vendorUser } };
        public static ObservableCollection<User> usersCollection = new ObservableCollection<User> { vendorUser };

        // Всякие юзер контролы, которые будут подставляться в окно.
        public static EditProductsPanel editProductPanel = new EditProductsPanel();
        public static ExportNecessaryProductsControl exportPanel = new ExportNecessaryProductsControl();
        public static CartPanel cartPanel = new CartPanel();
        public static ClientCabinet clientCabinet = new ClientCabinet();
        public static SellerCabinet sellerCabinet = new SellerCabinet();

        // Выбранный слева раздел.
        ClassificationSection activeSection = null;
        public ClassificationSection ActiveSection
        {
            get
            {
                return activeSection;
            }
            set
            {
                activeSection = value;
                if (value == null)
                {
                    editProductPanel.AddProductButton.Opacity = 0.5;
                }
                else
                {
                    editProductPanel.AddProductButton.Opacity = 0.9;
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }

        public ObservableCollection<Product> shownProducts = new ObservableCollection<Product>();
        public ObservableCollection<Product> ShownProducts 
        {
            get
            {
                if (activeSection is null) return null;
                return ActiveSection.ShownProducts;
            }
        }

        // Количество на складе. Если у товара значение меньше - их можно будет экспортировать в csv.
        public int MinProdutsToExport { get; set; } = 10;
        // Все товары, которые есть на складе. Используется для проверки уникальности артикула.
        public ObservableCollection<Product> AllProducts { get => TreeStructure.ClassificationSections[0].AllProductsInSubsections; }

        public MainWindow()
        {
            InitializeComponent();
            // Подписываемся на событие в User Control, вызываемое при изменении текущей секции, чтобы менять таблицу с товарами,
            // Так ей управляет главное окно.
            TreeStructureControl.ActiveSectionChanged += ChangeSection;

            DataContext = this;
        }

        // Обработчик события изменения раздела в дереве слева.
        private void ChangeSection(ClassificationSection newSection)
        {
            ActiveSection = newSection;
            // Если нужно отображать товары из всех подсекций текущей секции (пункт 6).
            if (ShowAllSubsectionProductsCheckBox.IsChecked == true)
            {
                ActiveSection.ShowAllProductsInSubsections = true;
            }
            else
            {
                ActiveSection.ShowAllProductsInSubsections = false;
            }
            // Обновляем коллекцию и обновляем ProductsTable.
            OnPropertyChanged("ShownProducts");
            ProductsTable.UpdateLayout();
        }
     
        // Обработчик события нажатия на кнопку сохранения текущего состояния склада в xml (серилизации).
        private void SaveStateButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Filter = "XML Data Files (*.xml)|*.xml",
            };
            if (saveFileDialog.ShowDialog() == false) return;

            try
            {
                Type[] knownTypes = new Type[] { typeof(Order), typeof(Product), typeof(User), typeof(VendorUser), typeof(ClassificationSection) };
                DataContractSerializer serializer1 = new DataContractSerializer(typeof(ObservableCollection<ClassificationSection>), knownTypes);
                DataContractSerializer serializer2 = new DataContractSerializer(typeof(ObservableCollection<User>), knownTypes);

                using (FileStream fileStream = new FileStream(saveFileDialog.FileName, FileMode.Create))
                {
                    serializer1.WriteObject(fileStream, TreeStructure.ClassificationSections);
                }

                using (FileStream fileStream = new FileStream(saveFileDialog.FileName + "_users", FileMode.Create))
                {
                    serializer2.WriteObject(fileStream, usersCollection);
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Не удалось cохранить текущее состояние в файл " + ex.Message,
                                "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            MessageBox.Show("Текущее состояние склада успешно сохранено", "Успех!", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        // Обработчик события нажатия на кнопку для загрузки состояния склада из файла (десерилизация).
        private void LoadStateButton_Click(object sender, RoutedEventArgs e)
        {
            activeSection = null;
            OnPropertyChanged("ShownProducts");
            ProductsTable.UpdateLayout();

            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "XML Data Files (*.xml)|*.xml",
            };
            if (openFileDialog.ShowDialog() == false) return;

            Type[] knownTypes = new Type[] { typeof(Order), typeof(Product), typeof(User), typeof(VendorUser), typeof(ClassificationSection) };
            DataContractSerializer serializer1 = new DataContractSerializer(typeof(ObservableCollection<ClassificationSection>), knownTypes);
            DataContractSerializer serializer2 = new DataContractSerializer(typeof(ObservableCollection<User>), knownTypes);

            try
            {
                using (FileStream fileStream = new FileStream(openFileDialog.FileName, FileMode.OpenOrCreate))
                {
                    var ClassificationSections = (ObservableCollection<ClassificationSection>)serializer1.ReadObject(fileStream);                    
                    TreeStructure.ClassificationSections = ClassificationSections;
                    TreeStructure.Tree.ItemsSource = TreeStructure.ClassificationSections;
                }

                using (FileStream fileStream = new FileStream(openFileDialog.FileName + "_users", FileMode.OpenOrCreate))
                {
                    usersCollection = (ObservableCollection<User>)serializer2.ReadObject(fileStream);
                    users = new Dictionary<string, User> { { vendorUser.Email, vendorUser } };
                    foreach (var user in usersCollection)
                    {
                        users[user.Email] = user;
                    }

                    sellerCabinet.OnPropertyChanged("Users");
                    sellerCabinet.OnPropertyChanged("UserOrders");
                }
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Не загрузить состояние склада из файла " + ex.Message,
                                "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            MessageBox.Show("Состояне склада успешно загружено!", "Успех!", MessageBoxButton.OK, MessageBoxImage.Information);

        }

        // Обработчик события изменения значение ЧекБокса, отвечающего за то, нужно ли показывать товары
        // из все подсекций текущей секции (пункт 6).
        private void ShowAllSubsectionProductsCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (ActiveSection is null) // Нужные настройки будут применены при смене секции.
            {
                return;
            }
             // Чтобы обновился ShownProducts.
            if (ShowAllSubsectionProductsCheckBox.IsChecked == true)
            {
                ActiveSection.ShowAllProductsInSubsections = true;
            }
            else
            {
                ActiveSection.ShowAllProductsInSubsections = false;
            }
            // Обновляем коллекцию и обновляем ProductsTable.
            OnPropertyChanged("ShownProducts");
            ProductsTable.UpdateLayout();
        }

        // Нужно поменять видимость кнопок панели.
        private void ProductsTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            editProductPanel.ProductsTable_SelectionChanged(sender, e);
        }

        // Авторизация.
        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            var loginWindow = new LoginWindow();
            if (loginWindow.ShowDialog() == true)
            {
                var email = loginWindow.Email;
                var password = loginWindow.Password;
                User user;

                try
                {
                    user = User.TryLogin(email, password);
                }
                catch (Exception exeption)
                {
                    MessageBox.Show(exeption.Message, "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                activeUser = user;

                if (user is VendorUser)
                {
                    UserPanel.Content = editProductPanel;
                    UserBottomPanel = exportPanel;
                    UserCabinet.Content = sellerCabinet;

                }
                else
                {
                    UserPanel.Content = cartPanel;
                    UserCabinet.Content = clientCabinet;
                }

                LoginButton.IsEnabled = false;
                LoginButton.Opacity = 0.5;
                RegistrationButton.IsEnabled = false;
                RegistrationButton.Opacity = 0.5;
                LogoutButton.IsEnabled = true;
                LogoutButton.Opacity = 1;
            }
        }
       
        // Регистрация.
        private void RegistrationButton_Click(object sender, RoutedEventArgs e)
        {
            var registrationWindow = new RegistrationWindow();
            if (registrationWindow.ShowDialog() == true)
            {
                User newUser;
                try
                {
                     newUser = User.TryCreate(registrationWindow.Name, registrationWindow.PhoneNumber, registrationWindow.Adress,
                                              registrationWindow.Email, registrationWindow.Password);
                }
                catch (Exception exeption)
                {
                    MessageBox.Show(exeption.Message, "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                users[newUser.Email] = newUser;
                usersCollection.Add(newUser);

                activeUser = newUser;
                UserPanel.Content = cartPanel;
                UserCabinet.Content = clientCabinet;

                LoginButton.IsEnabled = false;
                LoginButton.Opacity = 0.5;
                RegistrationButton.IsEnabled = false;
                RegistrationButton.Opacity = 0.5;
                LogoutButton.IsEnabled = true;
                LogoutButton.Opacity = 1;
            }
        }

        // Выход.
        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            LoginButton.IsEnabled = true;
            LoginButton.Opacity = 1;
            RegistrationButton.IsEnabled = true;
            RegistrationButton.Opacity = 1;
            LogoutButton.IsEnabled = false;
            LogoutButton.Opacity = 0.5;

            UserPanel.Content = null;
            UserBottomPanel.Content = null;
            UserCabinet.Content = null;

            activeUser = null;
        }

        // Сменился раздел у пользователя (кабинет или список товаров).
        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (activeUser is VendorUser)
            {
            }
            else
            {
                if (tabControl.SelectedIndex == 0)
                {
                    cartPanel.DeleteFromCartButton.Opacity = 0.3;
                    cartPanel.DeleteFromCartButton.IsEnabled = false;
                    cartPanel.AddToCartButton.IsEnabled = true;
                    cartPanel.AddToCartButton.Opacity = 1;
                }
                else if (tabControl.SelectedIndex == 1)
                {
                    cartPanel.AddToCartButton.Opacity = 0.3;
                    cartPanel.AddToCartButton.IsEnabled = false;
                    cartPanel.DeleteFromCartButton.IsEnabled = true;
                    cartPanel.DeleteFromCartButton.Opacity = 1;
                }
            }
        }
    }
}
